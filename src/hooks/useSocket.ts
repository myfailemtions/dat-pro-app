import { io } from 'socket.io-client'

const instance = io('https://u5v29.sse.codesandbox.io/')

export const useSocket = () => {
  const sendMessage = (message: any, channelName: string) =>
    instance.emit(channelName, message)

  return {
    sendMessage
  }
}
