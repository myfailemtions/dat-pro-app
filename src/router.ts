import { createWebHistory, createRouter, RouteRecordRaw } from 'vue-router'
import { useUserStore } from '@/stores'

const Home = () => import('@/pages/Home.vue')
const Login = () => import('@/pages/Login.vue')
const Track = () => import('@/pages/Track.vue')
const Claim = () => import('@/pages/Claim.vue')

const history = createWebHistory()

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    name: 'home',
    component: Home,
    beforeEnter: (_, __, next) => {
      const loggedIn = localStorage.getItem('token')
      if (loggedIn) {
        next('/track')
        return
      } else if (loggedIn) {
        next()
        return
      }
    }
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/track',
    name: 'track',
    component: Track
  },
  {
    path: '/claim/:id',
    name: 'claim-detail',
    component: Claim
  }
]

const router = createRouter({ history, routes })

router.beforeEach((to, from, next) => {
  const publicPages = ['/login', '/register', '/']
  const authRequired = !publicPages.includes(to.path)
  const loggedIn = localStorage.getItem('token')
  const needProcess = authRequired && !loggedIn
  // check if possible to fetch user Profile
  if (to.name !== 'login') {
    const userStore = useUserStore()
    userStore.init()
  }
  // trying to access a restricted page + not logged in
  // redirect to login page
  if (needProcess && to.name !== 'login') {
    next('/login')
    return
  } else {
    next()
    return
  }
})

export default router
