declare namespace DAT {
  type Coords = {
    lat: number
    lng?: number
    long?: number
  }

  type userInfo = {
    firstName: string
    lastName: string
    avatar: string
    coords: Coords
    currentDistance?: number
    id?: number
  }

  type Status = {
    [key: string]: number | string
  }

  type AuthError = {
    message: string
    code: number
  }

  type Avatar = {
    ext: string
    mime: string
    name: string
    url: string
    _id: string
  }

  type Profile = {
    username: string
    firstName: string
    lastName: string
    avatar: Avatar
    email: string
    partnerId: string
    customerNumber: string
    _id: string
  }

  type GenerateTokenRequest = {
    customerNumber: string
    user: string
    password: string
  }

  interface GenerateTokenResponse {
    token: string
    profile: Profile
  }

  type AuthForm = {
    customerNumber?: string
    user: string
    password: string
  }

  type Claim = {
    plateNo: string
    vin: string
    title: string
    type: string
    location: Coords
    image: string
    _id: string
    status: string
  }

  type PathFindResult = {
    nearest: DAT.userInfo[]
    farest: DAT.userInfo[]
  }
}

type Nullable<T> = {
  [P in keyof T]: T[P] | null
}

type ComponentSize = any

type SFCWithInstall = any

declare module 'vue3-google-map'
