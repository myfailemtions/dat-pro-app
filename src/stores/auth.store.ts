import { defineStore } from 'pinia'
import axios from '@/services/rest.service'

// compose stores
import { useUserStore } from './users.store'

type AuthStore = {
  loginError: DAT.AuthError | null
  loading: boolean
  token: string
}

export const useAuthStore = defineStore({
  id: 'auth',
  state: () =>
    <AuthStore>{
      loading: false,
      loginError: null,
      token: ''
    },
  getters: {
    errorMessage: ({ loginError }) => loginError,
    loggedIn: ({ token }) => !!token
  },
  actions: {
    setLoading() {
      this.loading = !this.loading
    },
    async login(creds: DAT.AuthForm) {
      try {
        this.setLoading()
        const userStore = useUserStore()
        const response = await axios<DAT.GenerateTokenResponse>({
          method: 'POST',
          url: 'api/v1/auth',
          data: creds
        })

        this.token = response.token
        userStore.profile = response.profile
        localStorage.setItem('token', response.token)
        localStorage.setItem('userId', response.profile._id)

        this.setLoading()
      } catch (err) {
        this.loginError = err
        throw new Error(err)
      }
    }
  }
})
