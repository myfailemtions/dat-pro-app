import { defineStore } from 'pinia'
import { Ref } from 'vue'
import { io } from 'socket.io-client'
import { fetchProfile } from '@/services/profile.service'
import DEFAULT_AVATAR from '@/assets/avatar.jpeg'

// Utils
import router from '@/router'

const SOCKET_URL = import.meta.env.VITE_SOCKET as string

type UserStore = {
  instance: any
  profile: DAT.Profile
  userId?: string | null
}

export const useUserStore = defineStore({
  id: 'users',
  state: () =>
    <UserStore>{
      instance: io(SOCKET_URL),
      profile: {},
      userId: ''
    },
  getters: {
    avatar: ({ profile }) =>
      profile.avatar ? `${SOCKET_URL}/${profile?.avatar?.url}` : DEFAULT_AVATAR
  },
  actions: {
    init() {
      this.userId = localStorage.getItem('userId')
      if (this.userId) {
        this.fetchProfile()
        return
      }
      this.logout()
    },
    logout() {
      router.push('/login')
    },
    sendPosition(coords: Ref<GeolocationCoordinates>) {
      this.instance.emit('track', {
        message: {
          ...this.profile,
          position: {
            lat: coords.value.latitude,
            lng: coords.value.longitude
          }
        }
      })
    },
    async fetchProfile() {
      try {
        if (this.userId) {
          this.profile = await fetchProfile(this.userId)
          return
        }
        this.logout()
      } catch (err) {
        this.logout()
      }
    }
  }
})
