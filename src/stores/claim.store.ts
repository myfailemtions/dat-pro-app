import { defineStore } from 'pinia'
import { fetchClaims, fetchClaim } from '@/services/claim.service'
import { ClaimStatus } from '@/types/enums'
import { useUserStore } from './users.store'

type ClaimStore = {
  claims: DAT.Claim[]
  claim: DAT.Claim
}

export const useClaimStore = defineStore({
  id: 'claims',
  state: () =>
    <ClaimStore>{
      claim: {},
      claims: [] as DAT.Claim[]
    },
  getters: {
    activeClaims: ({ claims }) =>
      claims.filter(({ status }) => status === ClaimStatus.Assigned),
    history: ({ claims }) =>
      claims.filter(({ status }) => status !== ClaimStatus.Assigned)
  },
  actions: {
    async fetchClaims() {
      try {
        const userStore = useUserStore()
        this.claims = await fetchClaims(userStore.profile.partnerId)
      } catch (err) {
        throw new Error(err)
      }
    },
    async fetchClaim(claimId: string) {
      try {
        this.claim = await fetchClaim(claimId)
      } catch (err) {
        throw new Error(err)
      }
    }
  }
})
