export const usersMock: DAT.userInfo[] = [
  {
    id: 1,
    firstName: 'John',
    lastName: 'Doe',
    avatar:
      'https://images.pexels.com/photos/1680172/pexels-photo-1680172.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
    coords: {
      lat: 51.68944143789777,
      lng: 5.312315970997394
    }
  },
  {
    id: 2,

    firstName: 'Bruce',
    lastName: 'Wayne',
    avatar:
      'https://images.pexels.com/photos/1222271/pexels-photo-1222271.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    coords: {
      lat: 51.726967601002414,
      lng: 5.29334700255126
    }
  },
  {
    id: 3,

    firstName: 'Samantha',
    lastName: 'Johns',
    avatar:
      'https://images.pexels.com/photos/733872/pexels-photo-733872.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    coords: {
      lat: 51.690528,
      lng: 5.2887722
    }
  }
]
