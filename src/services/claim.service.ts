import axiosFn from './rest.service'

const SOCKET_URL = import.meta.env.VITE_SOCKET as string

const SERVICE_URL = `${SOCKET_URL}/api/v1/claim`

export const fetchClaims = async (userId: string): Promise<DAT.Claim[]> => {
  const claim = await axiosFn<DAT.Claim[]>({
    method: 'GET',
    url: `${SERVICE_URL}/find?expert=${userId}`
  })
  return claim
}

export const fetchClaim = async (claimId: string): Promise<DAT.Claim> => {
  const claim = await axiosFn<DAT.Claim>({
    method: 'GET',
    url: `${SERVICE_URL}/${claimId}`
  })
  return claim
}
