import axiosFn from './rest.service'

const SOCKET_URL = import.meta.env.VITE_SOCKET as string

export const fetchProfile = async (userId: string): Promise<DAT.Profile> => {
  const profile = await axiosFn<DAT.Profile>({
    method: 'GET',
    url: `${SOCKET_URL}/api/v1/user/${userId}`
  })

  return profile
}
